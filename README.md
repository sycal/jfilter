# JFilter

A utility to query json documents.


`jfilter.py -h`  - Show help

`jfilter.py <json-doc> -c`  - Show

## Basic usage

    jfilter.py <json-doc> -c
    jfilter.py <json-doc> <object-path>

## Example object-paths (using `test.json` file)

- `/hosts/id=2` - Find all hosts with id=2.
- `/hosts/parents=3` - Find all hosts that have 3 in the parents list.
- `/hosts/name~web` - Find all hosts with a name matching the regex 'web'.
- `/hosts/0/commands` - Find the first host commands.
- `/hosts/-1/commands` - Find the last host commands.
- `/hosts/*/commands` - Find all host commands.
- `/hosts/*/@commands/name~cmd-2` - Find all hosts which have a command with name matching regex 'cmd-2'.
- `/hosts//@commands/name~cmd-2` - Alternative form of above command using empty string instead of an asterisk.
- `/state/0` - Find the first state list.
- `/state/0=1` - Find all state items where the first item in the state list equals 1.


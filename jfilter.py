#!/usr/bin/env python

from __future__ import print_function

import argparse
import json
import re
import sys

PY3 = sys.version_info[0] == 3

string_types = (str,) if PY3 else (str, unicode)


class InvalidPathError(Exception):
    pass


class FoundException(Exception):
    pass


def get_args(has_stdin=False):
    parser = argparse.ArgumentParser(description="Displays JSON data.", formatter_class=argparse.RawTextHelpFormatter)
    if not has_stdin:
        parser.add_argument('file', help="The JSON file path.")
    object_path_help_text = (
        "Object path to display, e.g.:\n"
        "\n/hosts/id=2                    - Find all hosts with id=2."
        "\n/hosts/parents=3               - Find all hosts that have 3 in the parents list."
        "\n/hosts/name~web                - Find all hosts with a name matching the regex 'web'."
        "\n/hosts/0/commands              - Find the first host commands."
        "\n/hosts/-1/commands             - Find the last host commands."
        "\n/hosts/*/commands              - Find all host commands."
        "\n/hosts/*/@commands/name~cmd-2  - Find all hosts which have a command with name matching regex 'cmd-2'."
        "\n/hosts//@commands/name~cmd-2   - Alternative form of above command using empty string instead of an '*'."
        "\n/state/0                       - Find the first state list."
        "\n/state/0=1                     - Find all state items where the first item in the state list equals 1."
    )
    parser.add_argument('object_path', help=object_path_help_text, nargs='?')
    parser.add_argument('-c', '--compact', help="Show compact summary view.", action='store_true')
    parser.add_argument('-s', '--case-sensitive', help="Case sensitive matching.", action='store_true')
    return parser.parse_args()


def q(text):
    return '"{}"'.format(text.replace('"', '\\"'))


def dump_compact_values(doc):
    if isinstance(doc, list):
        return '[.{}.]'.format(len(doc)) if doc else '[]'
    elif isinstance(doc, dict):
        return '{{}}'.format(len(doc)) if doc else '{}'
    elif isinstance(doc, string_types):
        return q(doc)
    else:
        return doc


def dump_compact_view(doc):
    indent = ' ' * 4
    if isinstance(doc, list):
        if doc:
            text = '['
            for value in doc:
                text += '\n{0}{1}'.format(indent, dump_compact_values(value))
            text += '\n]'
            return text
        else:
            return '[]'
    elif isinstance(doc, dict):
        if doc:
            text = '{'
            for key, value in doc.items():
                text += '\n{0}{1}: {2}'.format(indent, q(key), dump_compact_values(value))
            text += '\n}'
            return text
        else:
            return '{}'
    elif isinstance(doc, string_types):
        return q(doc)
    else:
        return doc


def show_json(root, path_parts, compact=False, flags=0, find=False):
    nfind = find
    try:
        if path_parts:
            part, remaining = path_parts[0], path_parts[1:]
            if part.startswith('@'):
                nfind = True
                part = part[1:]
            if isinstance(root, list):
                if '~' in part:
                    key, _, value = part.partition('~')
                    for selection in root:
                        actual = selection.get(key)
                        if actual is not None:
                            found = False
                            if isinstance(actual, list):
                                for l_actual in actual:
                                    if re.search(value, l_actual, flags=flags):
                                        found = True
                                        break
                            else:
                                found = re.search(value, actual, flags=flags)
                            if found:
                                if find:
                                    raise FoundException()
                                show_json(selection, remaining, compact=compact, flags=flags, find=nfind)
                elif '=' in part:
                    key, _, value = part.partition('=')
                    try:
                        int_key = int(key)
                    except ValueError:
                        int_key = None
                    for selection in root:
                        try:
                            actual = selection[int_key if int_key is not None else key]
                        except KeyError:
                            actual = None
                        if actual is not None:
                            found = False
                            if isinstance(actual, list):
                                for l_actual in actual:
                                    if str(l_actual).lower() == value.lower():
                                        found = True
                                        break
                            else:
                                found = (str(actual).lower() == value.lower())
                            if found:
                                if find:
                                    raise FoundException()
                                show_json(selection, remaining, compact=compact, flags=flags, find=nfind)
                else:
                    if part in ('*', ''):
                        for selection in root:
                            show_json(selection, remaining, compact=compact, flags=flags, find=nfind)
                    else:
                        try:
                            selection = root[int(part)]
                        except ValueError:
                            raise InvalidPathError("Expecting integer or '*' for path segment {0!r}".format(part))
                        show_json(selection, remaining, compact=compact, flags=flags, find=nfind)
            else:
                selection = root.get(part)
                if selection:
                    show_json(selection, remaining, compact=compact, flags=flags, find=nfind)
        elif compact:
            print(dump_compact_view(root))
        else:
            print(json.dumps(root, indent=4, sort_keys=True))
    except FoundException:
        if find:
            raise
        print(json.dumps(root, indent=4, sort_keys=True))


def main():
    has_stdin = not sys.stdin.isatty()
    args = get_args(has_stdin)
    if has_stdin:
        file_data = json.load(sys.stdin)
    else:
        with open(args.file) as f:
            file_data = json.load(f)
    selection = file_data
    object_path = args.object_path.strip('/') if args.object_path else None
    flags = 0 if args.case_sensitive else re.IGNORECASE
    if object_path:
        parts = object_path.split('/')
        show_json(selection, parts, compact=args.compact, flags=flags)
    else:
        show_json(selection, None, compact=args.compact, flags=flags)


if __name__ == '__main__':
    try:
        main()
    except InvalidPathError as e:
        print("Invalid path: " + e.message)
